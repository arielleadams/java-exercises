import java.util.Calendar;
import java.util.Scanner;

public class Student extends StudentBody {
    Scanner userinput = new Scanner(System.in);
    public String name() {
        String firstName = "First name";
        String lastName = "Last name";
        String updateFirstName = userinput.nextLine();
        String updateLastName = userinput.nextLine();

        firstName = updateFirstName;
        lastName = updateLastName;
        String name = firstName + " " + lastName;

        return name;
    }

    private String birthDate() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int minYear = year;
        int maxYear = year - 18;
        int monthOfBirth = 12;
        int dayOfBirth = 12;
        int yearOfBirth = maxYear; //default year is maximum birth year of enrollment

        int newMonthOfBirth = userinput.nextInt();
        int newDayOfBirth = userinput.nextInt();
        int newYearOfBirth = userinput.nextInt();

        monthOfBirth = newMonthOfBirth;
        dayOfBirth = newDayOfBirth;
        yearOfBirth = newYearOfBirth;

        return monthOfBirth + "/" + dayOfBirth + "/" + yearOfBirth;
    }

    private boolean enrollmentStatus() {
        boolean enrolled;
        char isEnrolled = userinput.next().charAt(0);

        if (isEnrolled == 'Y') {
            enrolled = true;
        } else {
            enrolled = false;
        }

        return enrolled;
    }

    private String averageGrade() {
        String gradeAverage = "A";
        String updateGradeAverage = userinput.nextLine();

        gradeAverage = updateGradeAverage;

        return gradeAverage;
    }

    public String studentInto() {
        System.out.println(name);
    }
}
